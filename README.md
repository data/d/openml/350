# OpenML dataset: webdata_wXa

https://www.openml.org/d/350

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: John Platt  
**Source**: [libSVM](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets) - Date unknown  
**Please cite**:   John C. Platt. 
Fast training of support vector machines using sequential minimal optimization. 
In Bernhard Schölkopf, Christopher J. C. Burges, and Alexander J. Smola, editors, Advances in Kernel Methods - Support Vector Learning, Cambridge, MA, 1998. MIT Press.a

This is the famous webdata dataset w[1-8]a in its binary version, retrieved 2014-11-14 from the libSVM site. Additional to the preprocessing done there (see LibSVM site for details), this dataset was created as follows: 

* load all web data  datasets, train and test, e.g. w1a, w1a.t, w2a, w2a.t, w3a, ... 
* join test and train for each subset, e.g. w1a and w1a.t, w2a and w2a.t 
* normalize each file columnwise according to the following rules: 
* If a column only contains one value (constant feature), it will set to zero and thus removed by sparsity. 
* If a column contains two values (binary feature), the value occuring more often will be set to zero, the other to one. 
* If a column contains more than two values (multinary/real feature), the column is divided by its std deviation.
* afterwards all these 8 files are merged into one, and randomly sorted. 
* duplicate lines were finally removed.

An R script which does all of these steps can be found here:
https://github.com/openml/data_scripts/blob/master/webdata_wXa/dataDownloader.R

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/350) of an [OpenML dataset](https://www.openml.org/d/350). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/350/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/350/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/350/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

